﻿using System;
using System.Collections.Generic;

namespace JuggleFest
{
    class Juggler
    {
        #region Members
            public String Name;
            public int H, E, P;
            public Dictionary<String, int> Preferences;
        #endregion

        #region Constructors
            public Juggler(String line)
            { // J J0 H:3 E:9 P:2 C2,C0,C1
                String[] data = line.Split(' ');
                if (data.Length != 6) throw new Exception("Invalid input string");

                Name = data[1];
                H = Int32.Parse(data[2].Substring(2));
                E = Int32.Parse(data[3].Substring(2));
                P = Int32.Parse(data[4].Substring(2));

                Preferences = new Dictionary<string, int>();
                foreach (String preference in data[5].Split(','))
                {
                    Preferences.Add(preference, 0);
                }
            }
        #endregion

        #region Methods
            public int getCompatabilityRating(Circuit compareC)
            {
                return (H * compareC.H) + (E * compareC.E) + (P * compareC.P);
            }
        #endregion
    }
}
