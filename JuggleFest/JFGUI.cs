﻿using System;
using System.Windows.Forms;

namespace JuggleFest
{
    public partial class frmJuggleFest : Form
    {
        Roster currentRoster;
        
        public frmJuggleFest()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog diaBrowse = new OpenFileDialog();
            diaBrowse.Filter = "Text Files (.txt)|*.txt";
            diaBrowse.FilterIndex = 1;
            diaBrowse.Multiselect = false;

            DialogResult userInteraction = diaBrowse.ShowDialog();
            if (userInteraction == DialogResult.OK)
            {
                txtFileLocation.Text = diaBrowse.FileName;

                try
                {
                    currentRoster = new Roster(diaBrowse.FileName);
                    txtCircuitList.Lines = currentRoster.toArray();
                    txtCalculate.Enabled = true;
                    btnCalculate.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error has occured." + Environment.NewLine + ex.Message);
                    return;
                }
            }
        }

        private void txtCircuitList_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtCircuitList.Text))
            {
                btnSave.Enabled = false;
            }
            else btnSave.Enabled = true;
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {

            try
            {
                Circuit c = currentRoster.getCircuitByID(txtCalculate.Text);
                MessageBox.Show(String.Format("The total juggler number for circuit {0} is {1}", c.Name, c.getJugglerTotal()));
            }
            catch (Exception ex) 
            {
                MessageBox.Show("An error has occured." + Environment.NewLine + ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog diaSave = new SaveFileDialog();
            diaSave.Filter = "Text Files (.txt)|*.txt";
            diaSave.FilterIndex = 1;

            DialogResult userInteraction = diaSave.ShowDialog();
            if (userInteraction == DialogResult.OK)
            {
                try
                {
                    System.IO.File.WriteAllLines(diaSave.FileName, currentRoster.toArray());                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error has occured." + Environment.NewLine + ex.Message);
                    return;
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult exit = MessageBox.Show("Any unsaved data will be lost. Close anyway?", "Exit", MessageBoxButtons.YesNo);
            if(exit == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
