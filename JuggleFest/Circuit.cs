﻿using System;
using System.Collections.Generic;

namespace JuggleFest
{
    class Circuit
    {
        #region Members
            public String Name;
            public int H, E, P;
            private LinkedList<Juggler> jugglerTeam;

            //Static members
            public static int MaxTeamSize = 0;
        #endregion

        #region Properties
            public Juggler[] JugglerTeam
            {
                get
                {
                    Juggler[] team = new Juggler[jugglerTeam.Count];
                    jugglerTeam.CopyTo(team, 0);
                    return team;
                }
            }
            public int TeamSize
            {
                get
                {
                    return jugglerTeam.Count;
                }
            }
            public int QualifyingScore
            {
                get
                {
                    return (jugglerTeam.Count > 0) ? jugglerTeam.Last.Value.getCompatabilityRating(this) : 0;
                }
            }
            private Juggler TrailingMember
            {
                get
                {
                    return jugglerTeam.Last.Value;
                }
            }
        #endregion

        #region Constructor
            public Circuit(string line)
            { // C C0 H:7 E:7 P:10
                String[] data = line.Split(' ');
                if (data.Length != 5) throw new Exception("Invalid input string");

                Name = data[1];

                H = Int32.Parse(data[2].Substring(2));
                E = Int32.Parse(data[3].Substring(2));
                P = Int32.Parse(data[4].Substring(2));

                jugglerTeam = new LinkedList<Juggler>();
            }
        #endregion

        #region Methods
            public bool tryAddMember(Juggler newMember, out Juggler bumpedMember)
            {
                bumpedMember = null;
                int newMemberRating = newMember.getCompatabilityRating(this);

                if ((jugglerTeam.Count >= MaxTeamSize) && (newMemberRating <= QualifyingScore))
                    //if newMember does not qualify...
                    return false;
                else if ((jugglerTeam.Count == 0) || (newMemberRating <= QualifyingScore))
                    //if the newMember qualifies but has the lowest score...
                    jugglerTeam.AddLast(newMember);
                else
                {
                    LinkedListNode<Juggler> i = jugglerTeam.First;
                    while (i != null)
                    {
                        int currentMemberRating = i.Value.getCompatabilityRating(this);
                        if (newMemberRating > currentMemberRating)
                        {
                            jugglerTeam.AddBefore(i, newMember);
                            break;
                        }
                        else i = i.Next;
                    }
                }

                if (jugglerTeam.Count > MaxTeamSize)
                {
                    bumpedMember = TrailingMember;
                    jugglerTeam.Remove(TrailingMember);
                }

                return true;
            }

            public int getJugglerTotal()
            {
                int total = 0;
                foreach (Juggler juggler in jugglerTeam)
                {
                    int jugglerNumber = 0;
                    if(Int32.TryParse(juggler.Name.Substring(1), out jugglerNumber))
                    {
                        total += jugglerNumber;
                    }
                }

                return total;
            }
        #endregion
    }
}
