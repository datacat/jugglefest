﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;

namespace JuggleFest
{
    class Roster
    {
        #region Members
            private Queue<Juggler> jugglers = new Queue<Juggler>();
            private Dictionary<String, Circuit> circuits = new Dictionary<String, Circuit>();
        #endregion

        #region Constructor
            public Roster(String textfileRoster)
            {
                populate(textfileRoster);
                generate();
            } 
        #endregion

        #region Methods
            private void populate(String strRosterFile)
            {
                StreamReader sr = new StreamReader(strRosterFile);
                String line;

                while ((!String.IsNullOrEmpty(line = sr.ReadLine())) && (line.Substring(0, 1).Equals("C")))
                {
                    Circuit newCircuit = new Circuit(line);
                    circuits.Add(newCircuit.Name, newCircuit);
                }

                if (circuits.Count == 0)
                {
                    sr.Close();
                    throw new Exception("Cannot continue program with an empty circuit list");
                }

                while ((!String.IsNullOrEmpty(line = sr.ReadLine())) && (line.Substring(0, 1).Equals("J")))
                {
                    Juggler newJuggler = new Juggler(line);
                    String[] preferences = new String[newJuggler.Preferences.Count];
                    newJuggler.Preferences.Keys.CopyTo(preferences, 0);

                    //set juggler preferences
                    foreach (String preference in preferences)
                    {
                        Circuit preferedCircuit;

                        if (circuits.TryGetValue(preference, out preferedCircuit))
                        {
                            newJuggler.Preferences[preference] = newJuggler.getCompatabilityRating(preferedCircuit);
                        }
                    }

                    jugglers.Enqueue(newJuggler);
                }

                if (jugglers.Count == 0) {
                    sr.Close();
                    throw new Exception("Cannot continue program with an empty juggler list"); 
                }
                else if (jugglers.Count % circuits.Count != 0)
                {
                    sr.Close();
                    throw new Exception("There cannot be an uneven distribution of jugglers");
                }
                else
                {
                    Circuit.MaxTeamSize = jugglers.Count / circuits.Count;
                    sr.Close();
                }
            }

            private void generate()
            {
                while (jugglers.Count > 0)
                {
                    Juggler newMember = jugglers.Dequeue();
                    Juggler bumpedMember;
                    Circuit selectedCircuit;
                    HashSet<String> PreferredCircuits = new HashSet<string>(newMember.Preferences.Keys);
                    PreferredCircuits.UnionWith(circuits.Keys);

                    foreach (String pref in PreferredCircuits)
                    {
                        bumpedMember = null;
                        selectedCircuit = null;
                        circuits.TryGetValue(pref, out selectedCircuit);

                        if (selectedCircuit.tryAddMember(newMember, out bumpedMember))
                        {
                            if (bumpedMember != null) 
                                jugglers.Enqueue(bumpedMember);
                            break;
                        }
                    }
                }
            }

            public String[] toArray()
            {//print circuit details into the text box
                String[] detailsArray = new String[circuits.Count];
                int index = 0;

                foreach(Circuit c in circuits.Values)
                {// C2 J6 C2:128 C1:31 C0:188, J3 C2:120 C0:171 C1:31, J10 C0:120 C2:86 C1:21, J0 C2:83 C0:104 C1:17
                    StringBuilder sb = new StringBuilder(c.Name);
                
                    foreach(Juggler j in c.JugglerTeam)
                    {
                        sb.AppendFormat(" {0}", j.Name);

                        foreach (KeyValuePair<String, int> preference in j.Preferences)
                        {
                            sb.AppendFormat(" {0}:{1}", preference.Key, preference.Value);
                        }

                        sb.Append(",");
                    }

                    sb.Remove(sb.Length - 1, 1); //remove the last comma

                    detailsArray[index] = sb.ToString();
                    index++;
                }

                Array.Reverse(detailsArray);

                return detailsArray;
            }

            public Circuit getCircuitByID(String strID) {
                Circuit viewCircuit = null;

                if (circuits.TryGetValue(strID.ToUpper(), out viewCircuit))
                {
                    return viewCircuit;
                }
                else throw new Exception(String.Format("Circuit {0} not found", strID));
            }
        #endregion
    }
}