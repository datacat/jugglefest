﻿namespace JuggleFest
{
    partial class frmJuggleFest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBrowse = new System.Windows.Forms.Label();
            this.txtFileLocation = new System.Windows.Forms.TextBox();
            this.lblInstructions = new System.Windows.Forms.Label();
            this.txtCircuitList = new System.Windows.Forms.TextBox();
            this.lblCircuitList = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCalculate = new System.Windows.Forms.Label();
            this.txtCalculate = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblBrowse
            // 
            this.lblBrowse.AutoSize = true;
            this.lblBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrowse.Location = new System.Drawing.Point(9, 52);
            this.lblBrowse.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrowse.Name = "lblBrowse";
            this.lblBrowse.Size = new System.Drawing.Size(84, 13);
            this.lblBrowse.TabIndex = 0;
            this.lblBrowse.Text = "File Location:";
            // 
            // txtFileLocation
            // 
            this.txtFileLocation.Location = new System.Drawing.Point(11, 68);
            this.txtFileLocation.Margin = new System.Windows.Forms.Padding(2);
            this.txtFileLocation.Name = "txtFileLocation";
            this.txtFileLocation.ReadOnly = true;
            this.txtFileLocation.Size = new System.Drawing.Size(445, 20);
            this.txtFileLocation.TabIndex = 2;
            // 
            // lblInstructions
            // 
            this.lblInstructions.AutoSize = true;
            this.lblInstructions.Location = new System.Drawing.Point(9, 7);
            this.lblInstructions.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(167, 26);
            this.lblInstructions.TabIndex = 3;
            this.lblInstructions.Text = "Welcome to JuggleFest\r\nLoad your roster file to get started!";
            // 
            // txtCircuitList
            // 
            this.txtCircuitList.Location = new System.Drawing.Point(11, 115);
            this.txtCircuitList.Margin = new System.Windows.Forms.Padding(2);
            this.txtCircuitList.Multiline = true;
            this.txtCircuitList.Name = "txtCircuitList";
            this.txtCircuitList.ReadOnly = true;
            this.txtCircuitList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCircuitList.Size = new System.Drawing.Size(525, 282);
            this.txtCircuitList.TabIndex = 4;
            this.txtCircuitList.WordWrap = false;
            this.txtCircuitList.TextChanged += new System.EventHandler(this.txtCircuitList_TextChanged);
            // 
            // lblCircuitList
            // 
            this.lblCircuitList.AutoSize = true;
            this.lblCircuitList.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCircuitList.Location = new System.Drawing.Point(9, 100);
            this.lblCircuitList.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCircuitList.Name = "lblCircuitList";
            this.lblCircuitList.Size = new System.Drawing.Size(71, 13);
            this.lblCircuitList.TabIndex = 5;
            this.lblCircuitList.Text = "Circuit List:";
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(420, 421);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(56, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(480, 421);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(56, 23);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(461, 66);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 9;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 682);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Circuit List:";
            // 
            // lblCalculate
            // 
            this.lblCalculate.AutoSize = true;
            this.lblCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalculate.Location = new System.Drawing.Point(9, 407);
            this.lblCalculate.Name = "lblCalculate";
            this.lblCalculate.Size = new System.Drawing.Size(309, 13);
            this.lblCalculate.TabIndex = 10;
            this.lblCalculate.Text = "Enter a Circuit ID for the sum of assigned Juggler IDs";
            // 
            // txtCalculate
            // 
            this.txtCalculate.Enabled = false;
            this.txtCalculate.Location = new System.Drawing.Point(12, 423);
            this.txtCalculate.Name = "txtCalculate";
            this.txtCalculate.Size = new System.Drawing.Size(130, 20);
            this.txtCalculate.TabIndex = 11;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Enabled = false;
            this.btnCalculate.Location = new System.Drawing.Point(148, 421);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 12;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // frmJuggleFest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(547, 455);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.txtCalculate);
            this.Controls.Add(this.lblCalculate);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCircuitList);
            this.Controls.Add(this.txtCircuitList);
            this.Controls.Add(this.lblInstructions);
            this.Controls.Add(this.txtFileLocation);
            this.Controls.Add(this.lblBrowse);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmJuggleFest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JuggleFest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBrowse;
        private System.Windows.Forms.TextBox txtFileLocation;
        private System.Windows.Forms.Label lblInstructions;
        private System.Windows.Forms.TextBox txtCircuitList;
        private System.Windows.Forms.Label lblCircuitList;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCalculate;
        private System.Windows.Forms.TextBox txtCalculate;
        private System.Windows.Forms.Button btnCalculate;
    }
}

